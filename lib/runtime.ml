module type LISA_RUNTIME = sig
  type index
  and 'a stream

  val int_of_index : index -> int
  val index_of_int : int -> index
  val fix : ((unit -> 'a) -> 'a) -> 'a

  val fixs :
    ((unit -> index -> 'a stream) -> index -> 'a stream) -> index -> 'a stream

  val head : 'a stream -> 'a
  val tail : 'a stream -> unit -> index -> 'a stream
  val cons : 'a -> (unit -> index -> 'a stream) -> 'a stream
  val nth : 'a stream -> int -> index -> 'a
  val at : (unit -> index -> 'a stream) -> (index -> index) -> index -> 'a
end

module MakePrelude (Impl : LISA_RUNTIME) = struct
  open Impl

  (* prelude *)
  let predi i = index_of_int (int_of_index i - 1)
  let now s i = at (fun () -> s) Fun.id i
  let pre2 s i = at s (fun i -> predi i |> predi) i
  let pre s i = at s predi i
  let succi i = index_of_int (int_of_index i + 1)
  let succ2i i = index_of_int (int_of_index i + 2)
  let o = index_of_int 0
  let ( |:: ) = cons
  let ( @ ) x y = at x y o
  let repeat x () = fixs (fun us i -> x () i |:: us)
  let mapl = List.map
  let cons = List.cons
  let mem_assoc = List.mem_assoc
end

module type TEST = functor (_ : LISA_RUNTIME) -> sig
  type t

  val stream : unit -> t
  val bench : int -> t -> unit
end
