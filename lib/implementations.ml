open Runtime

module DummyRuntime : LISA_RUNTIME = struct
  type 'a stream = unit
  and index = unit

  let index_of_int _ = assert false
  let int_of_index _ = assert false
  let cons _ _ = assert false
  let head _ = assert false
  let tail _ = assert false
  let fix _ = assert false
  let fixs = fix
  let nth _ _ _ = assert false
  let at _ _ _ = assert false
end

module ReferenceRuntime : LISA_RUNTIME = struct
  type 'a stream = Cons : 'a * (unit -> index -> 'a stream) -> 'a stream
  and index = int

  let index_of_int i = i
  let int_of_index i = i
  let cons x xs = Cons (x, xs)
  let head (Cons (x, _)) = x
  let tail (Cons (_, xs)) = xs

  let fix def =
    let rec t () = def t in
    t ()

  let fixs = fix

  let rec nth (Cons (x, xs)) n i =
    if n = 0 then x
    else
      let i' = i + 1 in
      nth ((xs ()) i') (n - 1) i'

  let at s from i = nth ((s ()) 0) (from i) 0
end

module LazyIndexRuntime : LISA_RUNTIME = struct
  type index = int

  module IndexedLazy = struct
    module IMap = Map.Make (struct
      type t = int

      let compare = Stdlib.compare
    end)

    type 'a w = { mutable results : 'a IMap.t; def : unit -> index -> 'a }

    let thunk def = { results = IMap.empty; def }

    let force ({ results; def } as thunk) i =
      match IMap.find_opt i results with
      | Some x -> x
      | None ->
          let v = def () i in
          let results' = IMap.add i v results in
          thunk.results <- results';
          v
  end

  open IndexedLazy

  type 'a stream = Cons : 'a * 'a stream w -> 'a stream

  let index_of_int i = i
  let int_of_index i = i
  let cons x xs = Cons (x, thunk xs)
  let head (Cons (x, _)) = x
  let tail (Cons (_, xs)) () = force xs

  let fix def =
    let rec t () = def t in
    t ()

  let fixs f =
    let rec x =
      { results = IMap.empty; def = (fun () -> f (fun () -> force x)) }
    in
    force x

  let rec nth (Cons (x, xs)) n i =
    if n = 0 then x
    else
      let i' = i + 1 in
      nth ((force xs) i') (n - 1) i'

  let at s from i = nth ((s ()) 0) (from i) 0
end

module IncrRuntime : LISA_RUNTIME = struct
  type 'a stream = {
    mutable shared : 'a pstream;
    offset : int;
    mutable initialized : bool;
    recdef : (unit -> index -> 'a stream) option;
  }

  and 'a pstream = {
    prefix : 'a Vector.t;
    mutable next_tail : unit -> index -> 'a stream;
    mutable label : access;
  }

  and access = InProgress of int | Done of int
  and index = int

  let index_of_int i = i
  let int_of_index i = i

  let cons head next_tail =
    let prefix = Vector.make 1 head in
    let offset = 0 in
    let initialized = true in
    let label = Done 0 in
    let shared = { prefix; next_tail; label } in
    { shared; offset; initialized; recdef = None }

  let prefix_length xs = Vector.length xs.shared.prefix - xs.offset

  let step xs i n_max =
    xs.shared.label <- InProgress i;
    let ys =
      let a = xs.shared.next_tail () i in
      if a == xs then (Option.get xs.recdef) () i else a
    in
    let len = min n_max (prefix_length ys) in
    Vector.blit ys.shared.prefix ys.offset xs.shared.prefix i len;
    xs.shared.label <- Done (i + len - 1);
    xs.shared.next_tail <- ys.shared.next_tail;
    i + len

  let unroll xs n =
    let limit = xs.offset + n in
    match xs.shared.label with
    | InProgress k when limit < k -> ()
    | Done k when k >= limit -> ()
    | Done k when n > 0 ->
        let x = ref (k + 1) in
        while !x <= limit do
          x := step xs !x limit
        done
    | _ -> failwith "cycle"

  let sforce xs i =
    match xs.initialized with
    | false when i = 0 ->
        (* init stream shared from 0 *)
        let t = xs.shared.next_tail () i in
        xs.shared <- t.shared;
        xs.initialized <- true;
        xs
    | true when i = 0 ->
        (* Share *)
        xs
    | true when i = xs.offset + 1 ->
        (* Force one step *)
        unroll xs 1;
        { xs with offset = xs.offset + 1 }
    | false -> xs.shared.next_tail () i
    | _ ->
        (* force stream with i *)
        (* xs.shared.next_tail () i *)
        raise (Invalid_argument "index out of sync")

  let head xs = Vector.get xs.shared.prefix xs.offset
  let tail xs () = sforce xs

  let fix def =
    let rec udef () = def udef in
    udef ()

  let fixs def =
    let rec a : 'a stream =
      let prefix = Vector.make 0 (Obj.magic 0) in
      let label = Done 0 in
      let next_tail () = def (fun () _ -> a) in
      let initialized = false in
      let offset = 0 in
      let shared = { prefix; next_tail; label } in
      {
        shared;
        offset;
        initialized;
        recdef = Some (fun () -> def (fun () _ -> a));
      }
    in
    sforce a

  let nth x n _ =
    unroll x n;
    Vector.get x.shared.prefix n

  let at s from i = nth ((s ()) 0) (from i) 0
end

let all : (string * (module LISA_RUNTIME)) list =
  [
    ("reference", (module ReferenceRuntime));
    ("lazy indexed", (module LazyIndexRuntime));
    ("fast", (module IncrRuntime));
  ]

let noref = List.filter (fun (name, _) -> name <> "reference") all

let fast : (string * (module LISA_RUNTIME)) list =
  [ ("fast", (module IncrRuntime)) ]

let reference : (string * (module LISA_RUNTIME)) list =
  [ ("ref", (module ReferenceRuntime)) ]

let lazyindexed : (string * (module LISA_RUNTIME)) list =
  [ ("lazy", (module LazyIndexRuntime)) ]

let dummy : (string * (module LISA_RUNTIME)) list =
  [ ("coiterative", (module DummyRuntime)) ]
