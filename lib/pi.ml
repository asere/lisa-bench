open Runtime

module Make : TEST =
functor
  (Impl : LISA_RUNTIME)
  ->
  struct
    module LisaPrelude = MakePrelude (Impl)
    open Impl

    (* prelude *)
    open LisaPrelude

    type t = index -> float stream

    let stream () =
      (* super acceleration of Pi *)
      let delta2 s =
        (repeat (fun () i ->
             let s () = s in
             let s0 = (at s Fun.id) i in
             let s1 = (at s succi) i in
             let s2 = (at s succ2i) i in
             s2 -. (((s2 -. s1) ** 2.) /. (s0 +. (-2. *. s1) +. s2))))
          ()
      in
      let sign = fixs (fun u_sign _ -> 1.0 |:: fun () _ -> -1.0 |:: u_sign) in
      let divs =
        fixs (fun u_divs _ -> 0.5 |:: repeat (fun () i -> 1.0 +. pre u_divs i))
      in
      let pi =
        fixs (fun u_pi _ ->
            4.
            |:: repeat (fun () i ->
                    let div = now divs i in
                    let summand = 2. /. div in
                    let prev = pre u_pi i in
                    let sign = now sign i in
                    let v = prev +. (sign *. summand) in
                    v))
      in
      let tableau =
        fixs (fun u_tableau i ->
            pi i
            |:: repeat (fun () i ->
                    let s = pre u_tableau i in
                    delta2 (fun _ -> s) i))
      in
      let super_pi = (repeat (fun () i -> head (now tableau i))) () in
      super_pi

    (* bench *)
    let bench n s =
      Sys.opaque_identity
        (ignore (at (fun () -> s) (fun _ -> index_of_int n) o))
  end
